# apdsh- ansible parallel distributed shell

Allows administrators to run commands against Ansible-managed infrastructure in parallel using inventory files.

## Setup

### Install Prerequisites
#### Ansible
Follow [instructions provided](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html) for your operating system.

#### jq
Most Linux distributions supply package `jq`. Binaries are provided at [this website](https://jqlang.github.io/jq/).

#### pdsh
Most Linux distribututions supply package 'pdsh'. Compiling the tool [is straightforward](https://github.com/chaos/pdsh) using the `--with-ssh` option to `./configure`.

### Install apdsh
```
git clone https://gitlab.com/certomodo/apdsh.git
cd apdsh
sudo cp apdsh /usr/local/bin
```

### Specify Inventory File
This assumes that you already have an Ansible inventory file. Documentation on how to write one is available [here](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html).

Add this to your `~/.bashrc` with the location of your inventory file.

```
export APDSH_INVENTORY=/path/to/inventory/file
```

## Usage

```
$ apdsh -h
Usage: ./apdsh -options
-i path to inventory file
  NOTE: can also use APDSH_INVENTORY environment variable
-g host group in ansible inventory file
-c command to run
-p number of ssh sessions to run in parallel
-h displays this message.
```

## Why use this instead of running Ansible playbooks?
### It's faster to run!

Immediate access from the command line rather than writing a playbook!

```
$ apdsh -i hosts -c uptime
host1:  18:22:29 up 173 days, 19:20,  0 users,  load average: 0.00, 0.00, 0.00
host2:  18:22:30 up 91 days, 23:15,  0 users,  load average: 0.06, 0.17, 0.19
```

### You can run long-running CLI tools!

#### Monitor resource utilization
```
$ ./apdsh -c dstat
host1: You did not select any stats, using -cdngy by default.
host1: --total-cpu-usage-- -dsk/total- -net/total- ---paging-- ---system--
host1: usr sys idl wai stl| read  writ| recv  send|  in   out | int   csw 
host1:   0   0 100   0   0|7011B   14k|   0     0 |  78B  181B| 144   338 
host2: You did not select any stats, using -cdngy by default.
host2: --total-cpu-usage-- -dsk/total- -net/total- ---paging-- ---system--
host2: usr sys idl wai stl| read  writ| recv  send|  in   out | int   csw 
host2:   4   1  95   0   0| 337B   82k|   0     0 |   0     0 |2524  4667 
host1:   0   0 100   0   0|   0     0 |  66B  366B|   0     0 | 135   325 
host2:   8   2  90   0   0|   0    12k| 107k   80k|   0     0 |2283  4083
```

#### Tailing logs
```
$ ./apdsh -g webservers -c "sudo tail -f /var/log/apache2/access.log"
```

### Can run bulk administration tasks!
**Warning: be careful when running commands like this en-masse in production!**

#### Fleet patching
```
$ ./apdsh -p 50 -c "sudo apt update"
```

#### Restarting services
```
$ ./apdsh -p 1 -c "sudo systemctl restart service"
```

## Limitations

### Does not support inventory aliases
`apdsh` extracts hostnames from the inventory file, meaning that the below example from the [Ansible documentation](https://docs.ansible.com/ansible/latest/inventory\_guide/intro\_inventory.html#inventory-aliases) will not work:

```
...
  hosts:
    jumper:
      ansible_port: 5555
      ansible_host: 192.0.2.50
```

### Assumes ssh key authentication
This tool does not support prompting for passwords.

